import React from 'react';
import logo from './logo.svg';
import 'bootstrap/dist/css/bootstrap.min.css';
import '@popperjs/core';
import './App.css';
import { Container, Row, Col } from 'react-bootstrap';
import ProductsContainer from './containers/productsContainer';
import CartContainer from './containers/cartContainer';

function App() {
    return (
        <div className="App" >
            {/* <div className="wrapper">
                <div className="productListing">
                    <ProductsContainer />
                </div>
                <div className="cartcontainer">
                    <CartContainer />
                </div>
            </div> */}
            <Container fluid={true} >
                <Row >
                    <Col sm={8} >
                        <ProductsContainer />
                    </Col>
                    <Col sm={4} >
                        <CartContainer />
                    </Col>
                </Row>
            </Container>
        </div>





    );
}

export default App;