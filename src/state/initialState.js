export const initialState = {
    user: {
        isLoggedIn: false,
        uname: ''
    },
    products: [
        {
            id: 1, name: 'Tshirts', price: 500, currency: '8377', gender: 'Male', qty: 1, desc: 'Tshirts for men only for 500', addedToCart: false
        },
        {
            id: 2, name: 'Tshirts', price: 800, currency: '8377', gender: 'Female', qty: 1, desc: 'Tshirts for women only for 800', addedToCart: false
        },
        {
            id: 3, name: 'Shirts', price: 900, currency: '8377', gender: 'Male', qty: 1, desc: 'Shirts for men only for 900', addedToCart: false
        },
        {
            id: 4, name: 'Shirts', price: 1200, currency: '8377', gender: 'female', qty: 1, desc: 'Shirts for women only for 1200', addedToCart: false
        },
        {
            id: 5, name: 'Jeans', price: 1400, currency: '8377', gender: 'female', qty: 1, desc: 'Jeans for women only for 1400', addedToCart: false
        },
        {
            id: 6, name: 'Jeans', price: 1500, currency: '8377', gender: 'Male', qty: 1, desc: 'Jeans for men only for 1500', addedToCart: false
        }

    ],
    cart: {
        total: 0,
        items: []
    }

};


