import { initialState } from '../state/initialState';
import { FETCH_ALL, FETCH_MALE, FETCH_FEMALE, UPDATE_PRODUCT_CART_STATUS } from '../actions/products-actions';




const productsReducer = (state = initialState.products, action) => {
    switch (action.type) {
        case FETCH_ALL:
            // console.log(action);
            // console.log(state);
            return state;
            break;
        case FETCH_MALE:
            // console.log(action);
            // console.log(state);
            return state;
            break;

        case FETCH_FEMALE:
            // console.log(action);
            // console.log(state);
            return state;
            break;
        case UPDATE_PRODUCT_CART_STATUS:
            return state.map((product, index) => (product.id === action.payload.id) ? { ...product, addedToCart: !product.addedToCart } : product);
            break;
        default:
            // console.log('default product');
            // console.log(action);
            // console.log(state);
            return state;
            break;
    }
}


export default productsReducer;