import { initialState } from '../state/initialState';
import { ADD_TO_CART, REMOVE_FROM_CART, UPDATE_CART } from '../actions/cart-actions';

const cartReducer = (state = initialState.cart, action) => {
    switch (action.type) {
        case ADD_TO_CART:
            console.log(ADD_TO_CART);
            return {
                ...state,
                total: state.total + action.payload.price,
                items: [...state.items, action.payload]
            }
            break;

        case REMOVE_FROM_CART:
            console.log(REMOVE_FROM_CART);
            return {
                ...state,
                total: state.total - action.payload.price,
                items: [
                    ...state.items.filter(item => item.id != action.payload.id)
                ]
            };
            break;

        case UPDATE_CART:
            // console.log(action);
            // console.log(state);
            return state;
            break;

        default:
            //console.log('default cart');
            // console.log(action);
            // console.log(state);
            return state;
            break;
    }
}




export default cartReducer;
