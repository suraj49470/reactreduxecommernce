export const FETCH_ALL = 'FETCH_ALL';
export const FETCH_MALE = 'FETCH_MALE';
export const FETCH_FEMALE = 'FETCH_FEMALE';
export const UPDATE_PRODUCT_CART_STATUS = 'UPDATE_PRODUCT_CART_STATUS';




export const FETCH_ALL_ACTION = () => ({
    type: FETCH_ALL
});

export const FETCH_MALE_ACTION = () => ({
    type: FETCH_MALE
});

export const FETCH_FEMALE_ACTION = () => ({
    type: FETCH_FEMALE
});


export const UPDATE_PRODUCT_CART_STATUS_ACTION = (product) => ({
    type: UPDATE_PRODUCT_CART_STATUS,
    payload : product
});

