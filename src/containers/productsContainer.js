import React, { Component } from 'react'
import { connect } from 'react-redux';
import Products from '../components/products'
import { FETCH_ALL_ACTION, FETCH_FEMALE, FETCH_MALE, UPDATE_PRODUCT_CART_STATUS_ACTION } from '../actions/products-actions'
import { ADD_TO_CART_ACTION, REMOVE_FROM_CART_ACTION } from '../actions/cart-actions';

class ProductsContainer extends Component {
    render() {
        return (
            <React.Fragment>
                <Products {...this.props} />
            </React.Fragment>
        )
    }
}


const mapStateToProps = (state) => {
    console.log(state)
    return {
        products: state.products
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        updateProductCartStatus: product => dispatch(UPDATE_PRODUCT_CART_STATUS_ACTION(product)),
        addToCart: product => dispatch(ADD_TO_CART_ACTION(product)),
        removeProductFromCart: product => dispatch(REMOVE_FROM_CART_ACTION(product))
    }
}



export default connect(mapStateToProps, mapDispatchToProps)(ProductsContainer);


