import React, { Component } from 'react'
import {connect} from 'react-redux';
import Cart from '../components/cart'

class CartContainer extends Component {
    render() {
        return (
            <React.Fragment>
                <Cart {...this.props}/>
            </React.Fragment>
        )
    }
}

const mapStateToProps = (state) => {
   return {
    cart : state.cart
   }
}

const mapDispatchToProps = (dispatch) => {
   return {
      
   }
}

export default connect(mapStateToProps , mapDispatchToProps)(CartContainer);