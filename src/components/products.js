import React from 'react'
import { CardColumns, Card, Button } from 'react-bootstrap';

function Products(props) {
    const { products, updateProductCartStatus, addToCart, removeProductFromCart } = props;
    return (
        <React.Fragment>
            <CardColumns>
                {
                    products.map((product, index) => (
                        <Card key={index}>
                            <Card.Img variant="top" src={"https://i.picsum.photos/id/" + (index + 12) + "/200/300.jpg"} />
                            <Card.Body>
                                <Card.Title>{product.name}  |  {String.fromCharCode(product.currency)}{product.price}</Card.Title>
                                <Card.Text>
                                    {product.desc}
                                </Card.Text>
                                <AddRemoveCartButton product={product} addToCart={addToCart} removeProductFromCart={removeProductFromCart} updateProductCartStatus={updateProductCartStatus}></AddRemoveCartButton>
                            </Card.Body>
                        </Card>
                    ))
                }

            </CardColumns>
        </React.Fragment>
    )
}





function AddRemoveCartButton(props) {
    const { addedToCart, updateProductCartStatus, addToCart , removeProductFromCart} = props;
    const { product } = props;
    const AddToCart = <Button onClick={() => { updateProductCartStatus(product); addToCart(product);}} variant="primary">Add to cart</Button>;
    const removeFromCart = <Button onClick={() => { updateProductCartStatus(product) ; removeProductFromCart(product) }} variant="danger">Remove</Button>;
    if (product.addedToCart) {
        return removeFromCart;
    } else {
        return AddToCart;
    }

}




export default Products

