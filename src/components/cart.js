import React from 'react'
import emptycart from '../images/emptycart.png'
import { Table, Card, Button } from 'react-bootstrap';
export default function Cart(props) {
    const { cart } = props;
    let emptycartcontainer;
    let cartdetailcontainer;
    let cartfooter;
    emptycartcontainer = cart.items.length <= 0 ? <EmptyCartComponent /> : undefined;
    cartdetailcontainer = cart.items.length > 0 ? <CartDetailComponent items={cart.items} /> : undefined
    cartfooter = cart.total > 0 ? <CartFooter total={cart.total} /> : undefined;
    return (
        <React.Fragment>
            <Card className="text-center cart-container">
                <Card.Header>Cart</Card.Header>
                {
                    emptycartcontainer
                }
                <Card.Body>
                    {
                        cartdetailcontainer
                    }
                </Card.Body>
                {
                    cartfooter
                }
            </Card>
        </React.Fragment>
    )
}





function CartDetailComponent(props) {
    console.log(props);
    const { items } = props;
    return (
        <React.Fragment>
            <Table responsive borderless={false} striped={true}>
                {/* <thead>
                    <tr>
                        <th>#</th>
                        <th>Table heading</th>
                    </tr>
                </thead> */}
                <tbody>
                    {
                        items.map((item, index) => (
                            <tr key={index}>
                                <td>
                                    <div>{item.name}</div>
                                    <div>{item.gender} | {String.fromCharCode(item.currency)}{item.price} x {item.qty}</div>
                                </td>
                                <td className="alignVertical">
                                    {
                                        String.fromCharCode(item.currency)
                                    }
                                    {
                                        item.price * item.qty
                                    }
                                </td>
                            </tr>
                        ))
                    }

                </tbody>
            </Table>
        </React.Fragment>
    );
}

function CartFooter(props) {
    console.log(props);
    const { total } = props;
    return (
        <React.Fragment>
            <Card.Footer className="text-bold">
                <p>
                    {String.fromCharCode(8377)}{total}
                </p>
                <Button variant="primary">Checkout</Button>
            </Card.Footer>
        </React.Fragment>
    );
}
function EmptyCartComponent(props) {
    return (
        <React.Fragment>
            <Card.Img variant="top" src={emptycart} />
        </React.Fragment>
    );
}
